public class CardException extends RuntimeException {
    /**
     * Create a CardException with the given message.
     */
    public CardException(String message) {
        super(message);
    }

}
