# Command Line Blackjack game

My implementation of a command line blackjack that is played by 1 user and an
automatic dealer. A player can win, lose or draw to the dealer via various
methods, which are recorded to give a tally of results at the end


# Why I made this programm

I made this programm as a project at the end of my first year of university.
It was great to learn some object orientated programming and start working
with java.


# Features

    * Players are dealt a car with the option to Hit or Stand.
    * If the players score exceeds or reaches 21, their turn is ended
        automatically.
    * Assuming the player did not go bust, the dealer will then draw cards
        until he either beats the player, or goes bust.
    * A score is kept of wins, draws and losses, which is shown to the player
        when they decide not to play another game.
    
    
# How to run on Linux

Set up directories as shown in my repository and cd into the src directory,
within the command line.

    * $ javac *.java
    * $ java Game
    

# Working Examples

Player gets 21:

![bj21](/images/blackjack-21.png)


Player stands and dealer goes bust:

![game2](/images/game2.png)


Player goes bust, final results are calculated and printed:

![finish](/images/finish.png)


# Specification

[Link 1: Specification](https://gitlab.com/lawebb/command-line-blackjack-game/-/blob/master/specification/cwk2.pdf)