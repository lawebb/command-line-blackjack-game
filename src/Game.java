import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * The class to run the game.
 *
 */
public class Game {

  /**
   * Main method for the game.
   */
  public static void main(String args[]) {

    char yn = 'y', c = 'h';     //Setting char variables to 'y' and 'h'
                                //for initial loops.
    int win = 0, draw = 0, lose = 0, round = 0;

    Deck deck = new Deck();

    BlackjackHand Phand = new BlackjackHand();
    BlackjackHand Dhand = new BlackjackHand();

    while(yn == 'y' || yn == 'Y'){

      round += 1;

      deck.shuffle();

      System.out.println("\nPlayer's Turn!\n");
      c = 'H';

      while(c == 'H' || c == 'h') {
        Card top = deck.deal();
        Phand.add(top);

        System.out.println(Phand.cards);

        //If the player goes bust the dealer wins.
        if(Phand.isBust()) {
          System.out.println("\nPlayer Has Gone Bust. DEALER WINS!\n");
          lose += 1;
          break;
        }

        else if(Phand.value() == 21) {
          c = 's';
          System.out.println("\nPlayer hand is 21!");
        }

        else {
          System.out.print("[H]it or [S]tand? ");

          Scanner sc = new Scanner(System.in);
          c = sc.next().charAt(0);

          //If a different character is entered, let them try again.
          while(! (c == 'h' || c == 'H' || c == 's' || c == 'S')){
            System.out.print("Please Enter A Correct Character.\n[H]it or [S]tand? ");

            sc = new Scanner(System.in);
            c = sc.next().charAt(0);
          }
        }
      }

      //If player hasn't already lost, go to the dealer's turn.
      if(! Phand.isBust()) {
        System.out.println("\nDealer's Turn!\n");

        while(Dhand.value() < 17){
          Card top = deck.deal();
          Dhand.add(top);

          System.out.println(Dhand.cards);

          //If the dealer goes bust, the player wins.
          if(Dhand.isBust()) {
            System.out.println("\nDealer Has Gone Bust. PLAYER WINS!\n");
            win += 1;
            break;
          }
        }

        //If neither player goes bust, calculate who has the winning hand.
        if(! Phand.isBust() && ! Dhand.isBust()) {
          if(Dhand.value() > Phand.value()){
            System.out.println("\nThe Dealer Has The Higher Hand. DEALER WINS!\n");
            lose += 1;
          }
          else if(Phand.value() > Dhand.value()) {
            System.out.println("\nThe Player Has The Higher Hand. PLAYER WINS!\n");
            win += 1;
          }
          else if(Dhand.value() == 21 && Phand.value() == 21) {
            if(Phand.isNatural() && Dhand.isNatural()) {
              System.out.println("\nBoth The Dealer And Player Have Natural Hands. IT IS A DRAW!\n");
              draw += 1;
            }
            else if(Dhand.isNatural()) {
              System.out.println("\nThe Dealer Has A Natural Hand. DEALER WINS!\n");
              lose += 1;
            }
            else if(Phand.isNatural()) {
              System.out.println("\nThe Player Has A Natural Hand. PLAYER WINS!\n");
              win += 1;
            }
            else{
              System.out.println("\nThe Dealer And Player Have The Same Value. IT IS A DRAW!\n");
              draw += 1;
            }
          }
          else if(Dhand.value() == Phand.value()) {
            System.out.println("\nThe Dealer And Player Have The Same Value. IT IS A DRAW!\n");
            draw += 1;
          }
        }
      }

      //If either hand still has cards, discard them to the deck.
      if(! Phand.isEmpty()){
        Phand.discard(deck);
      }

      if(! Dhand.isEmpty()){
        Dhand.discard(deck);
      }

      System.out.print("Play Again? [Y]/[N]: ");

      Scanner sc = new Scanner(System.in);
      yn = sc.next().charAt(0);

      //If a wrong character is entered, let the user try again.
      while(! (yn == 'y' || yn == 'Y' || yn == 'n' || yn == 'N')){
        System.out.print("Please Enter A Correct Character.\nPlay Again? [Y]/[N]: ");

        sc = new Scanner(System.in);
        yn = sc.next().charAt(0);
      }

      //Once the user prompts to end the game. Present them their game record.
      if(yn == 'n' || yn == 'N') {
        System.out.println("\nThankyou For Playing. Your Record Is Listed Below:");
        System.out.println("Rounds Played = " + round + "\nWins = " + win + "\nDraws = " + draw + "\nLosses = " + lose + "\n");
      }
    }
  }
}
