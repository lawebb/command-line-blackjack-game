import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * A class to represent a deck of cards.
 *
 * <p>This class will handle the creation, dealing and shuffling of
 * the deck of cards.</p>
 *
 */
public class Deck extends CardCollection {


  /**
   * Creates a deck of 52 cards in their natural sorted order.
   *
   * <p>This class creates a deck object of 52 cards, that is
   * sorted in to its natural order by suit and then by rank.<p>
   */
  public Deck() {
    Card.Rank[] rank = Card.Rank.values();
    Card.Suit[] suit = Card.Suit.values();

    for(int i=0; i<=3; i++) {
      for(int j=0; j<=12; j++) {
        Card card = new Card(rank[j], suit[i]);
        cards.add(card);
      }
    }
  }


  /**
   * Deals the top card of the deck.
   *
   * <p> Throws a cardException if the deck is empty. If it is not,
   * then the top card of the deck is dealt.
   *
   * @return The top card of the deck.
   */
  public Card deal() {
    if(cards.isEmpty()){
      throw new CardException("There are no cards left in the deck.");
    }

    Card top = cards.get(0);
    cards.remove(0);

    return top;
  }


  /**
   * Shuffles the deck.
   */
  public void shuffle() {
    Collections.shuffle(cards);
  }
}
