import java.util.List;
import java.util.LinkedList;


/**
 * A class to represent the blackjack hand.
 *
 * <p>The intent of the class is that it will be used to
 * handle the addition, subtraction and evaluation of a
 * blackjack hand.<p>
 *
 */
public class BlackjackHand extends CardCollection {


  /**
   * Creates an empty blackjack hand.
   */
  public BlackjackHand() {
  }


  /**
   * Takes a string input of cards and creates a blackjack hand out of them.
   */
  public BlackjackHand(String cards) {
    if(cards != null) {
      String hand[] = cards.trim().split("\\s+");

      for(int i=0; i < hand.length; i++){
        Card card = new Card(hand[i]);
        add(card);
      }
    }
  }


  /**
   * Discards all cards in the blackjack hand to the deck.
   *
   * <p> If the hand is empty, then the method with throw a cardException.
   * If it is not empty, the method will discard all cards in the hand and
   * return them to the deck.<p>
   *
   * @param deck The deck of cards to be returned to.
   */
  public void discard(Deck deck) {
    if(cards.isEmpty()) {
      throw new CardException("The hand is empty.");
    }
    while(! cards.isEmpty()){
      Card card = cards.get(0);
      deck.add(card);
      cards.remove(0);
    }
  }


  /**
   * Converts the set of cards in the blackjack hand to a string.
   *
   * @return The blackjack hand in string form.
   */
  public String toString() {
    String hand = "";

    for(int i=0; i<cards.size(); i++){
      Card next = cards.get(i);
      hand = hand + next;

      if(i != (cards.size() -1)){
        hand = hand + " ";
      }
    }

    return hand;
  }


  /**
   * Calculates the total value of the blackjack hand.
   *
   * <p>Adds the value of each card to the total value of the hand.
   * If there are Aces in the hand, it is determined whether they
   * need to be counted as 1 or 11.<p>
   *
   * @return The total value of the blackjack hand.
   */
  public int value() {
    int sum = 0;

    for(Card card: cards) {
      sum += card.value();
    }

    for(Card card: cards) {
      if(card.equals(new Card("AC")) || card.equals(new Card("AH")) || card.equals(new Card("AS")) || card.equals(new Card("AD"))) {
        if(sum + 10 < 22) {
          sum += 10;
        }
      }
    }
    return sum;
  }


  /**
   * Determines whether the blackjack hand is a natural hand.
   *
   * @return The boolean of whether it is true or false that the
   * hand is natural.
   */
  public boolean isNatural() {

    if(value() ==  21 && cards.size() == 2) {
        return true;
      }

      return false;
  }


  /**
   * Determines whether the blackjack hand is bust.
   *
   * @return The boolean of whether it is true or false that the
   * hand is bust.
   */
  public boolean isBust() {

    if(value() > 21) {
      return true;
    }

    return false;
  }


  /**
   * Adds a card to the blackjack hand.
   *
   * <p>Throws a cardException if the blackjack hand is already bust.
   * Otherwise, it adds the card to the hand.
   *
   * @param card The card to be added to the hand.
   */
  public void add(Card card) {
    if(isBust()) {
      throw new CardException("The hand is already bust.");
    }
    else{
      cards.add(card);
    }
  }
}
